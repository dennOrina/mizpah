import 'package:flutter/material.dart';
import 'package:mizpah/screens/shared/routes.dart';
import 'package:mizpah/screens/shared/strings.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  static MizpahText strings = new MizpahText();
  final MizpahRoutes routes = new MizpahRoutes();

  String title = strings.appTitle;
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: title,
      home: routes.splashScreen,
      routes:  routes.routes,
    );
  }
}

