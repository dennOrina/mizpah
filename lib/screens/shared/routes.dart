
import 'package:flutter/material.dart';
import 'package:mizpah/screens/authentication/login.dart';
import 'package:mizpah/screens/shared/splash.dart';

class MizpahRoutes {
  SplashScreen splashScreen;

  String homeRoute = '/home';  
  String splashScreenRoute = '/splash';
  String loginRoute = '/login';

  Map<String, WidgetBuilder> routes;

  MizpahRoutes() {
    splashScreen = new SplashScreen();
    routes = _buildRoutes();
  }

   Map<String, WidgetBuilder> _buildRoutes() {
    return <String, WidgetBuilder>{
      // homeRoute: (BuildContext context) => HomePage(),
      splashScreenRoute: (BuildContext context) => SplashScreen(),
      
      loginRoute: (BuildContext context) => LoginPage(),
      
    };
  }
}