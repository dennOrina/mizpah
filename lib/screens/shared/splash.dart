import 'dart:async';
import 'package:mizpah/screens/shared/routes.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  
  _SplashState createState()=> new _SplashState();
 
}

 class _SplashState extends State<SplashScreen> {

    @override
  void initState() {
    super.initState();

    // SystemChrome.setEnabledSystemUIOverlays([]);

    _startTimer();
  }

   MizpahRoutes routes = new MizpahRoutes();

   _startTimer() async {

     Duration timeout = Duration( seconds: 2,);

     Timer(timeout, (){
        Navigator.pushReplacementNamed(context, routes.loginRoute);
     });
   }

   @override
  Widget build(BuildContext context) {
    return Scaffold(body: Center(child: Text("Mizpah")));
  }
 }